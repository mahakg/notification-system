## Notification System using Lument laraval (Can be used like an API)

This is a very simple version of notification system. I've tried to design the architecture so that it can hadle the user notifications as well as content based notifications. We can also define the type of notifications. We can even handle the notifications that we want our application to send to users (In this case we'll set from_user as our system generated notififaction). It just needs on our business case what we want to achieve.

This is "only" tested on lumen development server i.e php artisan serv - I Could not directly use my apache server as it was conflicting with my existing apache settings. your localhost should run at http://localhost:8000/ , But I think it should work on your apache settings too



Set up migrations - php artisan migrate
Seed your DB's - php artisan db:seed

find unread notifications: http://localhost:8000/notifications/find (only get-> check routes)
create : http://localhost:8000/notifications/create -> only accept post
delete: http://localhost:8000/notifications/delete/{$id}

Test cases only for existing functionalities 
phpunit tests/NotificationsTest.php

It'll give the list of failed test cases in case of some db errors
