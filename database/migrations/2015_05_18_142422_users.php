<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * This table holds login data
 * for all users
 */
class Users extends Migration {

	/**
	 * Name of the database table
	 */
	private static $tableName = 'users';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::$tableName, function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

		  // our new fields
			$table->string('username')->unique();
			$table->string('password');
			$table->boolean('force_change_password')->default(True);
			$table->string('remember_token');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(self::$tableName);
	}

}
