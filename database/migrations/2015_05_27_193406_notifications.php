<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

//        Schema::create('users', function(Blueprint $table)
//        {
//            $table->increments('id');
//            $table->timestamps();
//
//            // our new fields
//            $table->string('username')->unique();
//            $table->string('password');
//            $table->boolean('force_change_password')->default(True);
//            $table->string('remember_token');
//        });


		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

            // OUr fields
            $table->string('notification_type');
            $table->string('notification_meta_type');
            $table->string('notification_meta_content_id');
            $table->string('notification_desc');

            $table->integer('sent_to_user')->unsigned();
            $table->integer('sent_from_user')->unsigned();
            $table->enum('is_read', array('Y', 'N'))->default('N');
            $table->foreign('sent_to_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('sent_from_user')->references('id')->on('users')->onDelete('cascade');;


//            $t->string('name');
//            $t->integer('age')->nullable();
//            $t->boolean('active')->default(1);
//            $t->integer('role_id')->unsigned();
//            $t->text('bio');


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('notifications');
	}

}
