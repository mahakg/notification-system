<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

include('UsersTableSeeder.php');
include('NotificationTableSeeder.php');

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UsersTableSeeder');
        $this->call('NotificationTableSeeder');

	}

}

