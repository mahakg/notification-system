<?php
/**
 * Created by PhpStorm.
 * User: mahak
 * Date: 27/5/15
 * Time: 10:11 PM
 */
use Illuminate\Database\Seeder;

class NotificationTableSeeder extends Seeder {

    public function run()
    {
        //delete users table records
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $records = DB::table('users')->get(['id']);
        $userid = array();
        foreach($records as $user_id_arr){
            $userid[] = $user_id_arr['id'];
        }
    //print_r($userid);
        DB::setFetchMode(PDO::FETCH_CLASS);
        //DB::table('notifications')->delete();
        echo "We are here";
        //insert some dummy records
        DB::table('notifications')->insert(array(
            array('notification_type'=>'Info','notification_desc'=>'Sample text1', 'created_at'=> date("Y-m-d H:i:s"), 'sent_from_user'=>$userid[0], 'sent_to_user'=>$userid[1], 'notification_meta_type'=>'', 'notification_meta_content_id'=>''),
            array('notification_type'=>'Error','notification_desc'=>'Sample text2', 'created_at'=> date("Y-m-d H:i:s"),'sent_from_user'=>$userid[0], 'sent_to_user'=>$userid[2],'notification_meta_type'=>'', 'notification_meta_content_id'=>''),
            array('notification_type'=>'Warn','notification_desc'=>'Sample text3', 'created_at'=> date("Y-m-d H:i:s"), 'sent_from_user'=>$userid[0], 'sent_to_user'=>$userid[3],'notification_meta_type'=>'', 'notification_meta_content_id'=>''),
            array('notification_type'=>'Warn','notification_desc'=>'Sample text4', 'created_at'=> date("Y-m-d H:i:s"), 'sent_from_user'=>$userid[0], 'sent_to_user'=>$userid[1],'notification_meta_type'=>'', 'notification_meta_content_id'=>''),
            array('notification_type'=>'Warn','notifications_desc'=>'Sample text5', 'created_at'=> date("Y-m-d H:i:s"), 'sent_from_user'=>$userid[0], 'sent_to_user'=>$userid[2],'notification_meta_type'=>'', 'notification_meta_content_id'=>''),
            array('notification_type'=>'Info','notification_meta_type'=>'Share', 'notification_meta_content_id'=>123, 'created_at'=> date("Y-m-d H:i:s"), 'sent_from_user'=>$userid[0], 'sent_to_user'=>$userid[1], 'notification_desc'=>'Sample text1' ),
            array('notification_type'=>'Info','notification_meta_type'=>'Like', 'notification_meta_content_id'=>123, 'created_at'=> date("Y-m-d H:i:s"), 'sent_from_user'=>$userid[2], 'sent_to_user'=>$userid[1],'notification_desc'=>'Sample text2'),
        ));
        echo "Are we here ?";
    }

}
