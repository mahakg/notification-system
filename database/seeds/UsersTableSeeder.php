<?php
/**
 * Created by PhpStorm.
 * User: mahak
 * Date: 27/5/15
 * Time: 10:11 PM
 */
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        //delete users table records
        DB::table('users')->delete();
        //insert some dummy records
        DB::table('users')->insert(array(
            array('username'=>'john','password'=>md5('PHPNinga'), 'created_at'=> date("Y-m-d H:i:s")),
            array('username'=>'mark','password'=>md5('JSNinga'), 'created_at'=> date("Y-m-d H:i:s")),
            array('username'=>'Karl','password'=>md5('JqueryNinga'), 'created_at'=> date("Y-m-d H:i:s")),
            array('username'=>'marl','password'=>md5('NotNinga'), 'created_at'=> date("Y-m-d H:i:s")),
            array('username'=>'mary','password'=>md5('HTMLNinga'), 'created_at'=> date("Y-m-d H:i:s")),
            array('username'=>'sels','password'=>md5('CSSNinga'), 'created_at'=> date("Y-m-d H:i:s")),
            array('username'=>'taylor','password'=>md5('LaravelNinga'), 'created_at'=> date("Y-m-d H:i:s")),

        ));
    }

}
