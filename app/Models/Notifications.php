<?php
# app/Models/Users.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notifications extends Model
{
    protected $table='notifications';

    protected $fillable = array('notification_type', 'notification_desc', 'sent_from_user', 'sent_to_user', 'notification_meta_type', 'notification_meta_content_id');
//('notification_type'=>'Info','notification_desc'=>'Sample text1', 'created_at'=> date("Y-m-d H:i:s"), 'sent_from_user'=>8, 'sent_to_user'=>8, 'notification_meta_type'=>'', 'notification_meta_content_id'=>'')


    protected $dates = ['deleted_at'];


}

