<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Controllers\Controller;
use App\Models\Notifications;
use Illuminate\Http\Request;
use Illuminate\Contracts\Database;


class NotificationsController extends Controller{

    public function index(){
        echo 123454;
        echo "Mahak Testing... This framework sucks!!";
        //$articles  = Article::all();

        //return response()->json($articles);

    }

    public function getNotifications(){

        try {
            $notifications = Notifications::where("is_read", 'N')->get();

            if ($notifications) {
                return response()->json(array('status' => true, 'data' => $notifications));
            } else {
                return response()->json(array('status' => false, 'error' => 'No data found'));
            }
        } catch (\Illuminate\Database\QueryException  $e){
            return response()->json(array('status' => false, 'error'=>$e));
        }
        //return response()->json($notifications);
    }

    public function createNotification(Request $request){
    //print_r($request->all());
        try {
            $notification = Notifications::create($request->all());
            if($notification){
                return response()->json(array('status' => true, 'data'=>$notification));
            }
        } catch (\Illuminate\Database\QueryException  $e){
            return response()->json(array('status' => false, 'error'=>$e));
        }
    }

    public function deleteNotification($id=""){
        try {
            if ($id == "") {
                return response()->json(array('status' => 'false', 'error' => 'No id passed to delete notification'));
            }
            $notification = Notifications::find($id);
            if ($notification) {
                $notification->delete();
                return response()->json(array('status' => true));
            } else {
                return response()->json(array('status' => false, 'error' => 'Nothing to delete'));
            }
        } catch(\Illuminate\Database\QueryException  $e){
            return response()->json(array('status' => false, 'error'=>$e));
        }



    }
}
