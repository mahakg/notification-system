<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$app->get('/', function() use ($app) {
    return $app->welcome();
});

$app->get('test','App\Http\Controllers\TestController@index');

$app->get('/notifications/find','App\Http\Controllers\NotificationsController@getNotifications');
$app->get('/notifications/delete/{id}','App\Http\Controllers\NotificationsController@deleteNotification');

$app->post('notifications/create','App\Http\Controllers\NotificationsController@createNotification' );
