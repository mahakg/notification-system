<?php

class NotificationsCase extends Laravel\Lumen\Testing\TestCase {

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function testFind(){
        $response = $this->call('GET', '/notifications/find');

        $content = json_decode($response->getContent(), true);
        print_r($content);
        if($content['status']){
            $this->assertEquals($content['status'], true);
        } else {
            echo $this->assertEquals($content['status'], false);

            echo $this->assertEquals($content['error'], 'No data found');
        }

    }

    public function testDelete(){
        $response = $this->call('GET', '/notifications/delete/1');

        $content = json_decode($response->getContent(), true);
        print_r($content);
        if($content['status']){
            echo $this->assertEquals($content['status'], true);
        } else{
            echo $this->assertEquals($content['status'], false);

            echo $this->assertEquals($content['error'], 'Nothing to delete');
        }

    }
    public function testCreate(){

        $data = array('notification_type'=>'Test_case_check','notification_desc'=>'Test_case_check', 'created_at'=> date("Y-m-d H:i:s"),
            'sent_from_user'=>8,
            'sent_to_user'=>8,
            'notification_meta_type'=>'', 'notification_meta_content_id'=>'');

        $response = $this->call('POST', '/notifications/create', $data);
        $content = json_decode($response->getContent(), true);

        print_r($content);
        $this->assertEquals($content['status'], true);


    }

}
